#!/bin/bash

mkdir -p /dev/net \
  && mknod /dev/net/tun c 10 200 \
  && chmod 600 /dev/net/tun

openvpn --config /usr/local/bin/client.ovpn --auth-user-pass <(echo -e "${1}\n${2}")